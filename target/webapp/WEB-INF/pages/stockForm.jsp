<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Stock Management Screen</title>
    </head>
    <body>
        <div align="center">
            <h1>New/Edit Stock</h1>
            <form:form action="saveStock" method="post" modelAttribute="stock">
                <form:hidden path="stockId"/>
                <form:hidden path="stockDailyRecords[0].recordId"/>
                <form:hidden path="stockDailyRecords[1].recordId"/>
                <table>
                    <tr>
                        <td>Code:</td>
                        <td><form:input path="stockCode" /></td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td><form:input path="stockName" /></td>
                    </tr>
                    <tr>
                        <td>1 - Price Open:</td>
                        <td><form:input path="stockDailyRecords[0].priceOpen" /></td>
                    </tr>
                    <tr>
                        <td>1 - Price Close</td>
                        <td><form:input path="stockDailyRecords[0].priceClose" /></td>
                    </tr>
                    <tr>
                        <td>2 - Price Open:</td>
                        <td><form:input path="stockDailyRecords[1].priceOpen" /></td>
                    </tr>
                    <tr>
                        <td>2 - Price Close:</td>
                        <td><form:input path="stockDailyRecords[1].priceClose" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><input type="submit" value="Save"></td>
                    </tr>
                </table>
            </form:form>
        </div>
    </body>
</html>