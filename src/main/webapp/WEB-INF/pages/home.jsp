<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Stock Management Screen</title>
    </head>
    <body>
        <div align="center">
            <h1>Stock List</h1>
            <h3>
                <a href="newStock">New Stock</a>
            </h3>
            
            <table border="1">
                <th>Code</th>
                <th>Name</th>
                <th>Daily Stock</th>
                <th>Actions</th>
                
                <c:forEach var="stock" items="${listStocks}">
                    <tr>
                        <td>${stock.stockCode}</td>
                        <td>${stock.stockName}</td>
                        <td>
                            <table border="1">
                                <th>Price Open</th>
                                <th>Price Close</th>
                                <c:forEach var="stockdaily" items="${stock.stockDailyRecords}">
                                <tr>
                                    <td>${stockdaily.priceOpen}</td>
                                    <td>${stockdaily.priceClose}</td>
                                </tr>
                                </c:forEach>
                            </table>
                        </td>
                        <td><a href="editStock?id=${stock.stockCode}">Edit</a>
                            <a href="deleteStock?id=${stock.stockId}">Delete</a
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>