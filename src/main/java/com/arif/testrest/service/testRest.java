/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arif.testrest.service;

import com.arif.testrest.model.RequestTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 *
 * @author hackerofgame
 */
@Controller
public class testRest {
    
    @RequestMapping(value = "/testing", method = RequestMethod.POST)
    public ResponseEntity<String> testingGetRest(@RequestBody RequestTest req ){
        return new ResponseEntity<String>(req.getInput(),HttpStatus.FOUND);
    }
}
