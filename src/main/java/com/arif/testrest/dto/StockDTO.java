/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arif.testrest.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author hackerofgame
 */
public class StockDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long stockId;
    private String stockCode;
    private String stockName;
    private List<StockDailyRecordDTO> stockDailyRecords;

    public StockDTO(Long stockId, String stockCode, String stockName, List<StockDailyRecordDTO> stockDailyRecords) {
        this.stockId = stockId;
        this.stockCode = stockCode;
        this.stockName = stockName;
        this.stockDailyRecords = stockDailyRecords;
    }

    public StockDTO() {
    }
    
    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public List<StockDailyRecordDTO> getStockDailyRecords() {
        return stockDailyRecords;
    }

    public void setStockDailyRecords(List<StockDailyRecordDTO> stockDailyRecords) {
        this.stockDailyRecords = stockDailyRecords;
    }

}
