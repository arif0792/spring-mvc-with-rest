/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arif.testweb.controller;

import com.arif.testrest.dto.StockDTO;
import com.arif.testrest.dto.StockDailyRecordDTO;
import com.arif.testweb.service.StockService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author hackerofgame
 */
@Controller
public class StockController {

    private static final Logger logger = Logger
            .getLogger(StockController.class);

    @Autowired
    private StockService stockService;

    @RequestMapping(value = "/")
    public ModelAndView listStocks(ModelAndView model) {
        List<StockDTO> listStocks = new ArrayList<>();
        try {
            listStocks = stockService.findAll();
            logger.fatal("test " + listStocks.toString());
        } catch (Exception e) {
            logger.fatal(e);
        }

        model.addObject("listStocks", listStocks);
        model.setViewName("home");
        return model;
    }

    @RequestMapping(value = "/newStock", method = RequestMethod.GET)
    public ModelAndView newContact(ModelAndView model) {
        StockDTO stockDTO = new StockDTO();
        model.addObject("stock", stockDTO);
        model.setViewName("stockForm");
        return model;
    }

    @RequestMapping(value = "/saveStock", method = RequestMethod.POST)
    public ModelAndView saveEmployee(@ModelAttribute StockDTO stock) {
        for(StockDailyRecordDTO stockDailyRecordDTO : stock.getStockDailyRecords()){
            stockDailyRecordDTO.setDate(new Date());
        }
        if (stock.getStockId() == null) { // if employee id is 0 then creating the
            // employee other updating the employee
            stockService.save(stock);
        } else {
            stockService.update(stock);
        }
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/deleteStock", method = RequestMethod.GET)
    public ModelAndView deleteEmployee(@RequestParam("id") Long id) {
        stockService.delete(id);
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/editStock", method = RequestMethod.GET)
    public ModelAndView editContact(@RequestParam("id") String id) {
        StockDTO stockDTO = stockService.findByStockCode(id);
        ModelAndView model = new ModelAndView("stockForm");
        model.addObject("stock", stockDTO);

        return model;
    }
}
