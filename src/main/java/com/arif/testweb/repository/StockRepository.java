/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arif.testweb.repository;

import com.arif.testweb.model.Stock;
import java.util.List;

/**
 *
 * @author hackerofgame
 */
public interface StockRepository {

    void save(Stock stock);

    void update(Stock stock);

    void delete(Stock stock);

    Stock findByStockCode(String stockCode);

    Stock findByStockId(Long id);

    List<Stock> findAll();
}
