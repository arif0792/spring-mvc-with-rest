/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arif.testweb.repository.hibernate;

import com.arif.testweb.model.Stock;
import com.arif.testweb.repository.StockRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hackerofgame
 */
@Transactional
public class StockRepositoryHibernate extends HibernateDaoSupport implements StockRepository {

    @Override
    public void save(Stock stock) {
        getHibernateTemplate().saveOrUpdate(stock);
    }

    @Override
    public void update(Stock stock) {
        getHibernateTemplate().saveOrUpdate(stock);
    }

    @Override
    public void delete(Stock stock) {
        getHibernateTemplate().delete(stock);
    }

    @Override
    public Stock findByStockCode(String stockCode) {
        List list = getHibernateTemplate().find(
                "from Stock where stockCode=?", stockCode
        );
        return (Stock) list.get(0);
    }
    
    @Override
    public Stock findByStockId(Long id) {
        List list = getHibernateTemplate().find(
                "from Stock where stockId=?", id
        );
        return (Stock) list.get(0);
    }

    @Override
    public List<Stock> findAll() {
        List<Stock> listStock = (List<Stock>) getHibernateTemplate().find(
                "from Stock");
        return listStock;

    }

}
