/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arif.testweb.service;

import com.arif.testrest.dto.StockDTO;
import java.util.List;

/**
 *
 * @author hackerofgame
 */
public interface StockService {

    void save(StockDTO stockDTO);

    void update(StockDTO stockDTO);

    void delete(Long id);

    StockDTO findByStockCode(String stockCode);
    
    List<StockDTO> findAll();
}
