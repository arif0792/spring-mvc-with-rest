/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arif.testweb.service.impl;

import com.arif.testrest.dto.StockDTO;
import com.arif.testrest.dto.StockDailyRecordDTO;
import com.arif.testweb.model.Stock;
import com.arif.testweb.model.StockDailyRecord;
import com.arif.testweb.repository.StockRepository;
import com.arif.testweb.service.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author hackerofgame
 */
public class StockServiceImpl implements StockService{
    
    StockRepository stockRepository;

    public void setStockRepository(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }
    
    @Override
    public void save(StockDTO stockDTO) {
        Stock stock = dtoStockToModel(stockDTO);
        stockRepository.save(stock);
    }

    @Override
    public void update(StockDTO stockDTO) {
        Stock stock = dtoStockToModel(stockDTO);
        stockRepository.update(stock);
    }

    @Override
    public void delete(Long id) {
        stockRepository.delete(stockRepository.findByStockId(id));
    }

    @Override
    public StockDTO findByStockCode(String stockCode) {
        return modelStockToDTO(stockRepository.findByStockCode(stockCode));
    }

    @Override
    public List<StockDTO> findAll() {
        List<Stock> listStock = stockRepository.findAll();
        List<StockDTO> listStockDTO = new ArrayList<>();
        for(Stock stock : listStock){
            listStockDTO.add(modelStockToDTO(stock));
        }
        return listStockDTO;
    }

    private Stock dtoStockToModel(StockDTO stockDTO) {
        return new Stock(stockDTO.getStockId(), stockDTO.getStockCode(), stockDTO.getStockName(), dtoStockDailyRecordToModel(stockDTO.getStockDailyRecords()));
    }

    private Set<StockDailyRecord> dtoStockDailyRecordToModel(List<StockDailyRecordDTO> stockDailyRecords) {
        Set<StockDailyRecord> stockDailyRecords1 = new HashSet<>();
        for(StockDailyRecordDTO stockDailyRecordDTO : stockDailyRecords){
            stockDailyRecords1.add(new StockDailyRecord(stockDailyRecordDTO.getRecordId(), stockDailyRecordDTO.getPriceOpen(), stockDailyRecordDTO.getPriceClose(), stockDailyRecordDTO.getPriceChange(), stockDailyRecordDTO.getVolume(), stockDailyRecordDTO.getDate()));
        }
        return stockDailyRecords1;
    }

    private StockDTO modelStockToDTO(Stock stock) {
        return new StockDTO(stock.getStockId(), stock.getStockCode(), stock.getStockName(), modelStockDailyRecordToDTO(stock.getStockDailyRecords()));
    }

    private List<StockDailyRecordDTO> modelStockDailyRecordToDTO(Set<StockDailyRecord> stockDailyRecords) {
        List<StockDailyRecordDTO> stockDailyRecords1 = new ArrayList<>();
        for(StockDailyRecord stockDailyRecord : stockDailyRecords){
            stockDailyRecords1.add(new StockDailyRecordDTO(stockDailyRecord.getRecordId(), stockDailyRecord.getPriceOpen(), stockDailyRecord.getPriceClose(), stockDailyRecord.getPriceChange(), stockDailyRecord.getVolume(), stockDailyRecord.getDate()));
        }
        return stockDailyRecords1;
    }
    
}
